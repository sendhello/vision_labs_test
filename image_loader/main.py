import os
import sys

import requests

URL = 'http://localhost/images'


def get_file_paths(folder_path: str) -> list[tuple[str, str]]:
    return [
        (file_name, os.path.join(folder_path, file_name))
        for file_name in os.listdir(folder_path)
    ]


if __name__ == '__main__':
    folder_path = sys.argv[1] if len(sys.argv) > 1 else None
    if folder_path is None:
        raise IOError('Не передан путь к папке с файлами')

    resp = requests.post(
        URL,
        files={
            file_name: open(file_path, 'rb')
            for file_name, file_path in get_file_paths(folder_path)
        }
    )
    if resp.ok:
        print("Файлы загружены")
    else:
        print("Error")
