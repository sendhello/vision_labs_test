import json
from typing import Union


def load_json(file_path: str) -> Union[dict, list]:
    with open(file_path) as f:
        return json.load(f)


def is_identical_float(a: float, b: float) -> bool:
    """Сравнение float чисел с точностью до 5 знаков.
    """
    if int(a * 100_000) == int(b * 100_000):
        return True

    return False


def is_identical(a: Union[dict, list], b: Union[dict, list], fild_name: str) -> tuple[bool, str]:
    """Сравнение словарей и списков.
    """
    if isinstance(a, list) and isinstance(b, list):
        if len(a) == len(b):
            for n, val_a in enumerate(a):
                res = is_identical(val_a, b[n], f'{fild_name}.{n}')
                if res[0] is False:
                    return False, res[1]

        else:
            return False, f'Размеры элементов <{fild_name}> различны'

    elif isinstance(a, dict) and isinstance(b, dict):
        if set(a.keys()) == set(b.keys()):
            for k, v in a.items():
                res = is_identical(a[k], b[k], f'{fild_name}.{k}')
                if res[0] is False:
                    return False, res[1]

        else:
            return False, f'Наборы элементов <{fild_name}> различны'

    elif isinstance(a, float) and isinstance(b, float):
        if not is_identical_float(a, b):
            return False, f'Элементы <{fild_name}> не равны'

    elif isinstance(a, (int, str, bool)) and isinstance(b, (int, str, bool)):
        if a != b:
            return False, f'Элементы <{fild_name}> не равны'

    elif a is None and b is None:
        pass

    else:
        return False, f'Разные структуры полей <{fild_name}>'

    return True, 'Структуры равны'


if __name__ == '__main__':
    file_a = load_json('json_compare/files/file_a.json')
    file_b = load_json('json_compare/files/file_b.json')

    print(is_identical(file_a, file_b, 'root'))
